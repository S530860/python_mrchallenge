# FOR EACH VIDEO, COUNT COMMENTS.
# RAW DATA HAS 1 COMMENT PER LINE.
# 
# STEP 1. MAP each line, output video id and 1 for this comment

# open data.csv for reading as a file stream named i
i = open("data.csv","r") 
# open m.csv for writing as a file stream named o
o = open("m.csv","w")
# for each line in your input file stream
count = 0
for line in i:  
  # strip the line, split it by the delimiter into a list named 'data'
  data = line.strip().split(",") 
  # if len(data) is equal to 4 (remember the colon)
  if (len(data) == 4):
    #Check whether the count is less than 5 and print the data in console
    count = count +1
    if count < 5 :
        print("{0}\t{1}\n".format(data[0], 1))

    # assign data to four named variables
    video_id,comment_text,likes,replies = data
    
    # use o.write to output the id comma 1
    o.write(video_id + "\t" + comment_text + "\n")
    print(video_id + "\t" + comment_text + "\n")

# close your file streams

i.close()
o.close()